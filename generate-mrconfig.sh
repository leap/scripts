#!/bin/sh
# Generate a .mrconfig containing all private repos
# adopted from https://github.com/svend/home-bin/blob/master/gen-mrconfig-priv


# Usage
# ./gen-mrconfig.sh GIT_REPO_URL
#
# Example
# ./gen-mrconfig.sh gitolite@some.ho.st


SSH=$1

gen_mrconfig ()
{
	while read s; do
		echo "[$(basename $s)]"
		echo "checkout = git clone ssh://$SSH/$s"
		echo
	done
}

#ssh $SSH expand |
ssh $SSH  |
# gitolite outputs list with DOS line endings
tr -d '\r' |
# only pick lines with space at the beginning
grep '^ ' |

# Last field contains the repo name
awk '{print $NF}' | 
# filter out all special gitolite groups, i.e. @all
grep -v '^@' |
 
# All private repos are under svend-priv/
#grep -e '^svend-priv/' -e '^gitolite-admin$' | 
gen_mrconfig |
# Delete last empty line
sed '$d'
