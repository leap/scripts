#!/bin/bash
# 
# example: 
# ./leap-fix.sh 3667
# Starting feature branch "3667_Sending_mail_fails_when_relaying_using_non-fully-qualified_hostname"
# ...

issue=$1

[ $issue -gt 0 ] || exit 1

# quick'n dirty
subject=`wget -q https://leap.se/code/issues/${issue}.json -O -| json_pp | grep '"subject"' | cut -d '"' -f 4 | sed "s/[][!@\'#\$\/%^&,\*() :]/_/g" |  cut -c -30`

[ $subject ] || exit 1

feature_default="${issue}_$subject"
#echo "Starting feature branch \"$feature\""
read -e -p "Enter feature branch name:" -i "$feature_default" feature

git flow feature start $feature
