# LEAP scripts repo

This repository contains some useful scripts and bits for sysdevs and general
purpose tools.
Please look into the seperate subdir `README.md` for further documentation.

## mrconfig

.mrconfig - to use this config the best thing to do is to put this in your
~/.mrconfig:

    [leap/scripts]
    checkout = git clone REPOURL scripts &&

    ln -s ${HOME}/leap/scripts/.mrconfig ${HOME}/leap/.mrconfig

that way this scripts repository will be checked out and the .mrconfig file will
be symlinked into your $HOME/leap directory.

You will also need to add the path to this .mrconfig to your ~/.mrtrust.

Then when in $HOME/leap, you can run 'mr checkout' and 'mr update' to checkout
all the repositories, and to get them updated.

NOTE: all of this is assuming a certain directory structure, so have a look at
that before you go ahead.
