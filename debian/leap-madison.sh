#!/bin/sh


usage() {
  echo "$0 <packagename>"
  echo "See which version of a LEAP package is included in which debian repo"
  exit 1
}

get_version() {
  repo="$1"
  dist="$2"
  component="$3"

  entry=$(curl -s "http://deb.leap.se/${repo}/dists/${dist}/${component}/binary-amd64/Packages" | grep -A 3 "^Package: ${name}")
  version=$(echo "${entry}" | grep '^Version' | cut -d':' -f 2 )
  echo "Repo ${repo}, dist ${dist}, component ${component}: ${version}"
}

name=$1

[ -z "$name" ] && usage

# Platform
for dist in master snapshots staging 0.10
do
  for component in jessie stretch
  do
    get_version platform ${dist} ${component}
  done
  echo
done
echo

# Client
for dist in master snapshots staging release
do
  for component in stretch buster sid zesty artful
  do
    get_version client ${dist} ${component}
  done
  echo
done
echo
