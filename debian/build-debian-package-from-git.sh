#!/bin/sh
# ad-hoc script for building a debian package from current
# git repo if there's no debian/ dir
#
# otherwise this might be sufficient:
#   debuild -uc -us
#
# Prerequisites:
#
#   apt install build-essential devscripts python-setuptools python-all dh-systemd


#DEBEMAIL="Leap Automatic Deb Builder <deb@leap.se>"
DEBEMAIL="Varac <varac@leap.se>"
DEBIAN_BRANCH='origin/debian/platform-0.9'

gitroot=$(git -C . rev-parse --show-toplevel)
version=$(cd "${gitroot}/client" && python setup.py version | grep Version | sed -e "s/.*: //")

tempdir=${gitroot}/dist
debdir=${tempdir}/debian_branch
branchdir=${tempdir}/current_branch

mkdir -p "${debdir}"
mkdir -p "${branchdir}"

GIT="git -C ${gitroot}"

${GIT} archive --format=tar ${DEBIAN_BRANCH} | tar -C "${debdir}" -x
${GIT} archive --format=tar HEAD | tar -C "${branchdir}" -x

cp -r "${debdir}/debian" "${branchdir}/"

(cd "${branchdir}" && DEBEMAIL="${DEBEMAIL}" dch -b --newversion "${version}" "Automatic build.")
(cd "${branchdir}" && debuild -uc -us)

echo "Packages and build artifacts can be found in: ${tempdir}"
ls ${tempdir}/*.deb
