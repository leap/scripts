#!/bin/sh


usage() {
  echo "$0 <repo> <dist> <component>"
  echo
  echo "Show which package versions are included in which debian repo"
  echo "Example: $0 platform snapshots jessie"
  exit 1
}

repo="$1"
dist="$2"
component="$3"

[ -z "$repo" ] && usage
[ -z "$dist" ] && usage
[ -z "$component" ] && usage

curl -s "http://deb.leap.se/${repo}/dists/${dist}/${component}/binary-amd64/Packages" | grep -A2 '^Package' | egrep -v '^(Architecture|Source)' | tr '\n' ' ' | sed 's/--/\n/g' | sed 's/^ *//'
