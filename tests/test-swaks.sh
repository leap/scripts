#!/bin/bash

local='dummy@bitmask.net'
nobody_local='fooooooo@bitmask.net'
external='dummy@leap.se'
server='leech.bitmask.net'
exit_code=0

# override $server with possible exported shell var
[ $SWAKS_SERVER ] && server=$SWAKS_SERVER

swaks_options="--timeout 5 --server $server --hide-informational --suppress-data"
swaks_auth="--tlsc --tls-cert $HOME/leap/test/certs/cert.crt --tls-key $HOME/leap/test/certs/cert.key"

echo "swaks_options: $swaks_options"
echo "swaks_auth   : $swaks_auth"
echo

test_swaks() {
  local from=$1
  shift
  local to=$1
  shift
  local expected_repsonse_code=$1
  shift
  local options="$@"

  response=`swaks -f $from -t $to $options | tail -3 | head -1`
  response_code=`echo "$response"| cut -d' ' -f 2-|sed 's/^ //g'|cut -d' ' -f 1 `
  if [ "$response_code" == "$expected_repsonse_code" ]
  then
    echo -n 'OK:      '
  else
    echo -n 'WARNING: '
    exit_code=1
  fi
 
  [[ "$options" == *--tls* ]] && authentication='authenticated    ' || authentication='no authentication'
  [  ]
  echo "From $from, to $to, $authentication: $response"
}


test_delivery() {
  local from=$1
  local to=$2
  local unauth_repsonse_code=$3
  local auth_response_code=$4 

  test_swaks $from $to $unauth_repsonse_code $swaks_options 
  test_swaks $from $to $auth_response_code $swaks_options $swaks_auth 

  echo
}


test_delivery $local $local 250 250
test_delivery $local $nobody_local 550 550
test_delivery $local $external 554 250

echo

test_delivery $nobody_local $local 250 250
test_delivery $nobody_local $nobody_local 550 550
test_delivery $nobody_local $external 554 250

echo

test_delivery $external $local 250 250
test_delivery $external $nobody_local 550 550
test_delivery $external $external 554 250

exit $exit_code
