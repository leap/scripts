#!/bin/sh

DOMAIN=$(hostname -d)

test_postmap () {
  echo -n "get $1, port 2244:"
  postmap -v -q $1 tcp:localhost:2244 2>&1 |grep recv:
  echo -n "get $1, port 4242:"
  postmap -v -q $1 tcp:localhost:4242 2>&1 |grep recv:
  echo
}

test_postmap dummy
test_postmap dummy@$DOMAIN
test_postmap dummy@nowhe.re

test_postmap fooooo
test_postmap fooooo@$DOMAIN
test_postmap fooooo@nowhe.re
