Quick shelr howto
=================

This is how to make automatic recordings without the hassle to type yourself (and fail and start over again).

Todo/Ideas
==========

* automatically split window and output all comments starting with ":" to one window, and all other cmds to another one

Before recording
----------------

* use zsh and _not_ bash, cause zsh+ohmyzsh autosets the window title to the running command 
* put DISABLE_CORRECTION="true" in .zshrc  (otherwise zsh might ask you for correction where it is not needed)
* turn off fancy shell prompts
* i use a custom "demo" user for recordings, with a clean .zshrc (see above) 

Setup shelr to use the ttyrec backend
-------------------------------------

I realized that the bad timing i encountered earlier was due the default shelr backend "script".
I changed it to "ttyrec" and now the timing is ok.
please try above shelr play cmd again with updated recording.
see [upload to shelr is currently broken](https://github.com/shelr/shelr/issues/21) 

    sudo apt-get install ttyrec
    shelr backend ttyrec


Record
------

* use max. 132x43 resolution in shell


    sudo su - demo

    rm -rf ~/leap/demo/example.org
    rm -rf ~/leap/demo/leap_cli
    rm -rf ~/leap/demo/leap_platform
 
    shelr record   # choose a short, intuitive title (use the first comment in the script) 


* in another termial start: 


     ./shelr-screencast.sh setup/setup.sh


Upload
======

* before uploading, edit "meta" and change following keys:

     `cd /home/demo/.local/share/shelr/RECORD_ID
     sed -i "s/`hostname`/demobox/g" meta`

* uploading sucks. you will always get an 500 error, but sometimes the vid yet shows up on http://shelr.tv. 
  i couldn't login using github oauth, if anyone have an openid account, please try.


     shelr setup xxxxxxxx   # setup upload api key (see pwstore)
     shelr push RECORD_ID

* Description: "see https://leap.se for more details"
* Tags: "leap, bitmask"
* you can always edit the description, tags and title later

Resetting demo machines
========================

* connect to the pistoncloud DC vpn, and souce your credentials for the 
* cd to your leap_pistonadmin working dir
* revert the instances using YOUR key !


    for h in chameleon panda seahorse; do ./revert_instance_from_snapshot.sh -i ${h}.testing.bitmask.net -s ${h}_gold -k $USER; done


* NOTE: you need to wait maybe 5 minutes for the nodes to be restarted

Preparing the demo machines
---------------------------

* make sure you can properly login to the machines with you ssh-key. use ssh-agent for it:


    eval `ssh-agent` && ssh-add

* to speed up initialization + deployment, login to the servers and:


     apt-get update && apt-get dist-upgrade

