: Leap Platform Demo, Part 2: Initializing and deploying nodes

cd ~/leap/demo/example.org
leap list
: Now we initialize all nodes, specifying the "production" tag which all nodes we created have assigned.
: Be sure to verify the ssh fingerprints !
leap node init production 

: Ok, lets delpoy !
leap --yes deploy

exit
