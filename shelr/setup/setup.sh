: Leap Platform Demo, Part1: Setting up a new provider

sudo /usr/bin/apt-get install git ruby ruby-dev rsync openssh-client openssl rake make

mkdir -p ~/leap/demo
cd ~/leap/demo

git clone https://leap.se/git/leap_cli.git
cd leap_cli
rake build
sudo /usr/bin/rake install

cd ..
git clone https://leap.se/git/leap_platform.git
cd leap_platform
git checkout develop 
git submodule sync
git submodule update --init

cd ..
mkdir example.org
cd example.org
leap new --contacts admin@example.org --domain example.org --name example --platform /home/demo/leap/demo/leap_platform .
leap add-user --self
leap cert ca
leap cert csr
leap cert dh

leap node add web1 services:webapp ip_address:199.119.112.23 tags:production
leap node add vpn1 services:openvpn ip_address:199.119.112.24 openvpn.gateway_address:199.119.112.25 tags:production
leap node add couch1 services:couchdb ip_address:199.119.112.26 tags:production

leap compile
leap list
ls -l

exit
