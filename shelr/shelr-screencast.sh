#!/bin/bash
# by varacanero@zeromail.org
# under gpl licence v3

# Usage: 

# * check if wait_for_prompt() is working correctly by running shelr-screencast.sh ~/coding/xdotool/script
# * in order to get this working, you need sth like this in your .bashrc: 
#   PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
# * open another terminal, not bigger than 132x43
# * gain temp. sudo priviledges with "sudo /bin/true" 
# * start "shelr record"
# * start this script in another terminal i.e. "./shelr-screencast.sh /home/varac/coding/xdotool/script" 
# * move mouse focus to shelr window !

# Limitations
# to query if a window is processing some cmd of is waiting with a shell prompt, 
# you can't su to another users, cause xprop would not recognize running cmds.
# you need to run this as the xsession user


# for tty backend ~350
DELAY_MAX=350

fake_input() {
  line="$@"
  for (( i=0; i<${#line}; i++ )); do
    delay=`shuf -i 12-$DELAY_MAX -n 1`
    xdotool type --delay $delay  "${line:$i:1}"
  done
  xdotool key Return
}

wait_for_prompt() {
  no_prompt=true
  while $no_prompt 
  do
    sleep 1
    name=`win_name`
    echo "$name"|grep -q "$prompt_regexp" && no_prompt=false
  done
  sleep 0.3 
}

win_name () {
  local name
  name="`xprop -id $win_id WM_NAME | cut -d'"' -f 2`"
  echo $name
}


# main


# begin

echo
echo "Move mouse over to the recording terminal !"
sleep 3 


file=$1
win_id=`xprop -root | grep '_NET_ACTIVE_WINDOW(WINDOW)' | cut -d' '  -f5`

# for zsh:
#prompt_regexp="^$USER@`hostname`:"
prompt_regexp="demo@rocinante:"

# bash:
#prompt_regexp="^$USER:.*\$"

while read line           
do           
  fake_input "$line" 
  wait_for_prompt
done < $file


