#!/bin/sh

# Use this sed script to clean your logs before
# pasting it publicly

# Usage:
#   Replace `your_username` and `your_domain` in this script, then:
#
#     ./clean-log.sh LOGFILE

# Todo:
#  - IPs (should be already cleaned by rsyslog)
#  - Browser user agents
#  - Generate this script from leap_cli

sed -r '
# Clean all email addresses
s/((\w|[.])+)@((\w|[.])+)/<EMAIL REDACTED>/g

# Replace username
s/your_username/<USERNAME_REDACTED>/g

#Replace domain
s/your_domain/<DOMAIN_REDACTED>/g
' "$1"
