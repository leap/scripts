s/|varac/|Varac/
s/|micah/|Micah/
s/|elijah/|Elijah/
s/|drebs/|Drebs/
s/|Micah Anderson/|Micah/
s/|Tomas Touceda/|Tomás/
s/|kwadro.aut/|Kwadronaut/
s/|Kali Kaneko/|Kali/
s/|Isis Agora Lovecruft/|Isis/
s/|Parménides GV/|Parmegv/

# remove all submodule commits before first leap_platform commit
1,/leap_platform/d
