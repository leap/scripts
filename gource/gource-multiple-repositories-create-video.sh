#!/usr/bin/env bash
# Generates gource video (h.264) out of multiple repositories.
# Pass the repositories in command line arguments.
# Example:
# <this.sh> /path/to/repo1 /path/to/repo2

outfile="leap-combined.webm"
config='leap_platform_combined.conf'
tmpvid='/tmp/tmpgource.webm'
audio='fire_at_will.mp3'

time gource $combined_log --load-config $config -o - |  ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libvpx -b 10000K $tmpvid
rm $outfile
ffmpeg -i $tmpvid -i $audio -map 0 -map 1 -vcodec copy -shortest $outfile
