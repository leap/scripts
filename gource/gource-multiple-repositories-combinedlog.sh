#!/usr/bin/env bash
# Generates gource video (h.264) out of multiple repositories.
# Pass the repositories in command line arguments.
# Example:
# <this.sh> /path/to/repo1 /path/to/repo2


# too many repos do not have develp
#BRANCH='develop'


GITDIR='/home/varac/leap/git'

#REPOS='bitmask bitmask_drebs duplicity-backup-status gitolite_hare gitolite_orca keymanager leap_android leap_assets leap_ca leap_cli leap_client leap_client_dist leap_client_launcher leap_cloudadmin leap_db leap_doc leap_mail leap_mx leap_pistonadmin leap_platform leap_platform_dev leap_platform_errors leap_pycommon leap_vagrant leap-vagrant leap_web leap_website leap_website_pages minimal-debian-vagrant nickserver platform_test puppet puppet_apache puppet_apt puppet_augeas puppet_backupninja puppet_bundler puppet_concat puppet_couchdb puppet_gcc puppet_git puppet_gpg puppet_haproxy puppet_munin puppet_nagios puppet_passenger puppet_puppet puppet_python puppet_reprepro puppet_ruby puppet_rubygems puppet_shorewall puppet_stdlib puppet_strongswan puppet_stunnel puppet_unbound puppet_user puppet_x509 pwstore pysqlcipher python_gnutls python_jsonschema python_ping python_requests python_sh python_srp ruby_srp scripts soledad soledad_client soledad_server srp_js testing testytest thandy'
# only leap_cli, leap_platform + submodules
REPOS='leap_cli leap_platform_gource puppet_apache puppet_bundler puppet_git puppet_nagios puppet_reprepro puppet_squid  puppet_stunnel puppet_apt  puppet_concat puppet_gpg puppet_passenger puppet_ruby puppet_squid_deb_proxy puppet_unbound puppet_augeas puppet_couchdb puppet_haproxy puppet_puppet puppet_rubygems puppet_stdlib  puppet_user puppet_backupninja puppet_gcc puppet_munin puppet_python puppet_shorewall puppet_strongswan puppet_x509'
#REPOS=`find $GITDIR -maxdepth 1 -mindepth 1 -type d -printf '%p '`

USERFILTER='(^Aaron DeVore$|^Adrien Thebo$|^Alex Cline$|^am$|^Amos Shapira$|^anarcat$|^andreas$|^Andreas$|^Andreas Maag$|^Andreas Zuber$|^Antoine Beaupre$|^Antoine Beaupré$|^arne$|^Arne Schwabe$|^Ashley Penney$|^ausiv4$|^ben c$|^Ben Carrillo$|^Benjamin Krause$|^Branan Purvine-Riley$|^Brandon Helwig$|^Chad Metcalf$|^Chris Jones$|^Chris Price$|^Chris Spence$|^Christian G. Warden$|^Christian Kaenzig$|^cprice$|^crayfishx$|^Dan Bode$|^dann$|^david$|^David Schmitt$|^Devin Theriot-Orr$|^dudu$|^duritong$|^Eric Shamow$|^Eric Sorenson$|^Erik Dalén$|^Erik Froese$|^Eugene Kirpichov$|^Evan Stachowiak$|^fatmcgav$|^fiddyspence$|^Francois Deppierraz$|^Frederic Junod$|^Gabriel Filion$|^Garrett Honeycutt$|^Gary Larizza$|^Gildas Le Nadan$|^gitolite on orca$|^git repository hosting$|^Hailee Kenney$|^Hunter Haugen$|^intrigeri$|^Jaka Hudoklin$|^James Fryman$|^James Turnbull$|^Jamie McClelland$|^Jan Baier$|^Jaromil$|^Jeff McCune$|^Jerome Charaoui$|^Joe Julian$|^Jonathan Boyett$|^Joshua Harlan Lifton$|^Joshua Hoblitt$|^Juerg Gerber$|^Justin Lambert$|^Keith aka fugit$|^Ken Barber$|^Kristof Willaert$|^Krzysztof Wilczynski$|^ktdann$|^Luke Kanies$|^Marcel$|^Marcel Haerry$|^Marc Fournier$|^Mark Smith-Guerrero$|^Markus Heberling$|^martin f. krafft$|^Martin Hellmich$|^Mathieu Bornoz$|^Matthaus Owens$|^Matthias Imsand$|^Matt Taggart$|^mh$|^Mickaël Canévet$|^nadir$|^Nan Liu$|^nfagerlund$|^ng$|^Nick Groenen$|^o$|^Oded Simon$|^Pascal Simon$|^Patrick Carlisle$|^Paul Tötterman$|^Peter Meier$|^peter woodman$|^Philipp Gassmann$|^Philip Potter$|^Pierre-Yves Ritschard$|^pietro$|^Pietro$|^Pietro Ferrari$|^Raphaël Pinson$|^Reid Vandewiele$|^Richard Clamp$|^Richard Soderberg$|^R.I.Pienaar$|^Riseup$|^Riseup Labs$|^Rohan Sheth$|^Roman Barczyński$|^root$|^R. Tyler Croy$|^Ryan Coleman$|^Salimane Adjao Moustapha$|^Sander Hoentjen$|^sathlan$|^schwabe$|^Sergey Stankevich$|^Sharif Nassar$|^Silvio Rhatto$|^Simon Josi$|^stephen$|^Steve Huff$|^Steve Traylen$|^Stig Sandbeck Mathisen$|^Tails developers$|^Theo Spears$|^Thomas Bartelmess$|^Tim$|^Uwe Stuehler$|^Wil Cooley$|^woldan$|^wwwEagle$|^Will Farrington$|^Alex O.*Rielly$|^Chris Boot$|^Hubert$|^Paweł Tomulik$|^Rafael Gálvez Vizcaíno$|^Tomas Doran$)'


i=0

for repo in $REPOS; do
  echo "processing $repo"
  # 1. Generate a Gource custom log files for each repo. This can be facilitated by the --output-custom-log FILE option of Gource as of 0.29:
  logfile="$(mktemp /tmp/gource.XXXXXX)"
  #gource --output-custom-log --user-filter "$USERFILTER" "${logfile}" ${repo}
  #gource --user-filter "$USERFILTER" --output-custom-log "${logfile}" ${GITDIR}/${repo}
  gource --user-filter "$USERFILTER" --output-custom-log "${logfile}" ${GITDIR}/${repo}
  # 2. If you want each repo to appear on a separate branch instead of merged onto each other (which might also look interesting), you can use a 'sed' regular expression to add an extra parent directory to the path of the files in each project:
  sed -i -E "s#(.+)\|#\1|/${repo}#" ${logfile}

  logs[$i]=$logfile
  let i=$i+1
done

#combined_log="$(mktemp /tmp/gource.XXXXXX)"
combined_log="combined.log"

cat ${logs[@]} | sort -n > $combined_log
rm ${logs[@]}

sed -f clean_combinedlog.sed -i $combined_log


echo "Committers:"
cat $combined_log | awk -F\| {'print  $2'} | sort | uniq
echo "======================"
