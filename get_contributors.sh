#!/bin/bash

# Script to generate a list of contributors
# Included: All repos from https://leap.se/git/?o=age that show activity since 2y
# Excluded: puppet_*

REPOS=(bitmask_android bitmask_bundler bitmask_client bitmask-dev bitmask_help bitmask_launcher bitmask_net bitmask_thunderbird bonafide couchdb_scripts couchrest_changes deploy_test dl_bitmask_net docker faith keymanager leap_assets leap_ca leap_cli leap_client_launcher leap_cloudadmin leap_db leap_doc leap-keyring leap_mail leap_mx leap_pistonadmin leap_platform leap_presentations leap_pycommon leap_se leap_vagrant leap_web leap_website libsodium lut mail_breaker nickserver packer_vagrant platform_test pymemoryhole pysqlcipher python_gnupg-ng python_pycryptopp python_srp python_zmq reserve_usernames ruby_srp scripts soledad soledad-perf sqlcipher srp_js tapicero version_report )

# shellcheck disable=SC2016
GET_CONTRIBUTORS='git log --branches --not --remotes=subrepo --use-mailmap --format="%aN <%aE>" | awk "{arr[\$0]++} END{for (i in arr){print arr[i], i;}}" | sort -rn | cut -d" " -f2- | sort'
ROOTDIR="${HOME}/leap/git"

# optional specify core team memebers to filter out
TEAM='(email@of.contributor.org|email@of_other_contributor.net)'

declare -A contributors

for repo in "${REPOS[@]}"
do
  cd "${ROOTDIR}/${repo}" || exit 1
  # Uncomment for first run to make sure all repos are up to date
  # git fetch
  contributors_repo=$(eval "$GET_CONTRIBUTORS" | egrep --text -iv "$TEAM")
  if [ -n "$contributors_repo" ]
  then
    echo "${repo}:"
    while read -r line; do
      echo "$line"
      contributors["$line"]+="$repo, "
    done <<< "$contributors_repo"
    echo
    echo
  fi
done

echo
echo

for contributor in "${!contributors[@]}"; do echo "$contributor: ${contributors[$contributor]}"; done | sort
